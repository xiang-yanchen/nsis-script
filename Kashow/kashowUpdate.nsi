﻿; 该脚本使用 HM VNISEdit 脚本编辑器向导产生

  !include "x64.nsh"
  !include "nsProcess.nsh"

Unicode true

; 安装程序初始定义常量
!define PRODUCT_NAME "kashow"
!define PRODUCT_VERSION "1.0.6"
!define PRODUCT_PUBLISHER "Annew"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\kashow.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

SetCompressor lzma

; ------ MUI 现代界面定义 (1.67 版本以上兼容) ------
!include "MUI.nsh"

; MUI 预定义常量
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; 语言选择窗口常量设置
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

; ; 欢迎页面
; !insertmacro MUI_PAGE_WELCOME
; 安装目录选择页面
!define MUI_PAGE_CUSTOMFUNCTION_PRE SelectRootDir



; 安装过程页面
!insertmacro MUI_PAGE_INSTFILES
; 安装完成页面
; !define MUI_FINISHPAGE_RUN "$INSTDIR\kashow.exe"
; !insertmacro MUI_PAGE_FINISH



; 安装卸载过程页面
; !insertmacro MUI_UNPAGE_INSTFILES

; 安装界面包含的语言设置
; !insertmacro MUI_LANGUAGE "English"
; !insertmacro MUI_LANGUAGE "SimpChinese"

; 安装预释放文件
!insertmacro MUI_RESERVEFILE_LANGDLL
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS
; ------ MUI 现代界面定义结束 ------
!define /date PRODUCT_DATE %Y%m%d

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
; OutFile "kashow_Upgrades_${PRODUCT_DATE}.exe"
OutFile "kashow_Upgrades.exe"
InstallDir "$PROGRAMFILES64\kashow"
ShowInstDetails show
ShowUnInstDetails show

; 安装位置
Function SelectRootDir
  StrCpy $INSTDIR "D:\kashow"
FunctionEnd

Section "MainSection" SEC01
  ;静默安装 silent
  SetSilent silent 
  SetOutPath "$INSTDIR"
  ; SetOverwrite :   ifnewer 跳过  on 覆盖
  SetOverwrite on
  File /r ".\kashowUpdate\*.*"
  File ".\kashowUpdate\kashow.exe"

  ; 安装完成
  ExecShell "" "$INSTDIR\${PRODUCT_NAME}.exe"

  ; 结束安装程序
  Quit
SectionEnd

/******************************
 *  以下是安装程序的卸载部分  *
 ******************************/

Section Uninstall
  Delete "$INSTDIR\kashow\kashow.exe"
  Delete "$INSTDIR\kashow\startClient.exe"
  Delete "$INSTDIR\kashow\html"
  Delete "$INSTDIR\kashow\sys.ini"


SectionEnd


#-- 根据 NSIS 脚本编辑规则，所有 Function 区段必须放置在 Section 区段之后编写，以避免安装程序出现未可预知的问题。--#
Function .onInit
  ; !insertmacro MUI_LANGDLL_DISPLAY
  ; 先关闭以前的程序
  ${nsProcess::FindProcess} "${PRODUCT_NAME}.exe" $R0
  IntCmp $R0 0 running no_running
  running:
  ; MessageBox MB_ICONQUESTION|MB_YESNO "安装程序检测到 ${PRODUCT_NAME} 正在运行，是否强行关闭并继续安装?" IDYES dokill IDNO stopit
  GoTo dokill
  no_running:
  GoTo endding
  dokill:
  nsProcess::_CloseProcess "kashow.exe"
  Pop $R0
  GoTo endding
  stopit:
  Abort
  endding:
  nsProcess::_Unload
FunctionEnd

; Function .onInstSuccess
;   ExecShell "" "$INSTDIR\${PRODUCT_NAME}.exe"
; FunctionEnd