﻿; 该脚本使用 HM VNISEdit 脚本编辑器向导产生

  !include "x64.nsh"
  !include "nsProcess.nsh"

Unicode true

; 安装程序初始定义常量
!define PRODUCT_NAME "kashow"
!define PRODUCT_VERSION "1.3.9"
!define PRODUCT_PUBLISHER "Annew"
!define PRODUCT_WEB_SITE "https://kashow-1307730918.cos.ap-nanjing.myqcloud.com/kashow.png"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\kashow.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

SetCompressor lzma

; ------ MUI 现代界面定义 (1.67 版本以上兼容) ------
!include "MUI.nsh"

; MUI 预定义常量
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; 语言选择窗口常量设置
!define MUI_LANGDLL_REGISTRY_ROOT "${PRODUCT_UNINST_ROOT_KEY}"
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

; 欢迎页面
!insertmacro MUI_PAGE_WELCOME
; 许可协议页面
!insertmacro MUI_PAGE_LICENSE ".\kashow\license.txt"
; 安装目录选择页面
!define MUI_PAGE_CUSTOMFUNCTION_PRE SelectRootDir


; 安装过程页面
!insertmacro MUI_PAGE_INSTFILES
; 安装完成页面
!define MUI_FINISHPAGE_RUN "$INSTDIR\startClient.exe"
!insertmacro MUI_PAGE_FINISH

; 安装卸载过程页面
!insertmacro MUI_UNPAGE_INSTFILES

; 安装界面包含的语言设置
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "SimpChinese"

; 安装预释放文件
!insertmacro MUI_RESERVEFILE_LANGDLL
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS
; ------ MUI 现代界面定义结束 ------
!define /date PRODUCT_DATE %Y%m%d

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "kashow_x64_${PRODUCT_DATE}.exe"
InstallDir "$PROGRAMFILES64\kashow"
ShowInstDetails show
ShowUnInstDetails show

Var UNINSTALL_PROG
Var OLD_VER
Var OLD_PATH

Section "MainSection" SEC01
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File /r ".\kashow\*.*"
  CreateShortCut "$DESKTOP\KaShow.lnk" "$INSTDIR\startClient.exe"
  File ".\kashow\kashow.exe"
  Call AutoBoot ; 安装完成后调用自启动函数
SectionEnd

Section -AdditionalIcons
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateDirectory "$SMPROGRAMS\kashow"
  CreateShortCut "$SMPROGRAMS\kashow\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\kashow\Uninstall.lnk" "$INSTDIR\uninst.exe"
  CreateShortCut "$SMPROGRAMS\kashow\KaShow.lnk" "$INSTDIR\startClient.exe"
 
 ;create desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\startClient.exe" ""

SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

#-- 根据 NSIS 脚本编辑规则，所有 Function 区段必须放置在 Section 区段之后编写，以避免安装程序出现未可预知的问题。--#
; 安装位置
Function SelectRootDir
  StrCpy $INSTDIR "D:\kashow"
FunctionEnd
Function .onInit
  !insertmacro MUI_LANGDLL_DISPLAY
	;先关闭以前的程序
  
  ${nsProcess::FindProcess} "${PRODUCT_NAME}.exe" $R0
  ${If} $R0 == 0
    MessageBox MB_OK "安装程序检测到‘${PRODUCT_NAME}’ 正在运行，请关闭之后再安装！"
    Quit
  ${EndIf}
 
  ;先停止服务，等待3s
  ClearErrors
  ReadRegStr $UNINSTALL_PROG ${PRODUCT_UNINST_ROOT_KEY} ${PRODUCT_UNINST_KEY} "UninstallString"
  IfErrors  done

  ReadRegStr $OLD_VER ${PRODUCT_UNINST_ROOT_KEY} ${PRODUCT_UNINST_KEY} "DisplayVersion"

  MessageBox MB_YESNO|MB_ICONQUESTION "检测到本机已经安装了 ${PRODUCT_NAME} $OLD_VER。 $\n$\n是否卸载旧版以继续安装？"  /SD IDYES \
      IDYES uninstall \
      IDNO cancel

cancel:
  Abort
  
uninstall:
  StrCpy $OLD_PATH $UNINSTALL_PROG -10
  ExecWait '"$UNINSTALL_PROG" /S _?=$OLD_PATH' $0
  Delete "$UNINSTALL_PROG"
  RMDir $OLD_PATH

done:
FunctionEnd

Function AutoBoot
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Run" "kashow"'"$INSTDIR\startClient.exe"'
FunctionEnd

/******************************
 *  以下是安装程序的卸载部分  *
 ******************************/

Section Uninstall

  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\uninst.exe"
  Delete "$INSTDIR\kashow.exe"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Website.lnk"
  Delete "$SMPROGRAMS\kashow"
  Delete "$SMPROGRAMS\kashow\*.*"
  Delete "$SMPROGRAMS\kashow\*.*"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}"
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  RMDir "$SMPROGRAMS\kashow"

  RMDir /r "$INSTDIR\virtualkeyboard"
  RMDir /r "$INSTDIR\translations"
  RMDir /r "$INSTDIR\tools"
  RMDir /r "$INSTDIR\styles"
  RMDir /r "$INSTDIR\scenegraph"
  RMDir /r "$INSTDIR\resources"
  RMDir /r "$INSTDIR\QtWinExtras"
  RMDir /r "$INSTDIR\QtWebView"
  RMDir /r "$INSTDIR\QtWebEngine"
  RMDir /r "$INSTDIR\QtWebChannel"
  RMDir /r "$INSTDIR\QtTest"
  RMDir /r "$INSTDIR\QtQuick3D"
  RMDir /r "$INSTDIR\QtQuick.2"
  RMDir /r "$INSTDIR\QtQuick"
  RMDir /r "$INSTDIR\QtQml"
  RMDir /r "$INSTDIR\QtMultimedia"
  RMDir /r "$INSTDIR\QtGraphicalEffects"
  RMDir /r "$INSTDIR\Qt"
  RMDir /r "$INSTDIR\qmltooling"
  RMDir /r "$INSTDIR\position"
  RMDir /r "$INSTDIR\playlistformats"
  RMDir /r "$INSTDIR\platforms"
  RMDir /r "$INSTDIR\platforminputcontexts"
  RMDir /r "$INSTDIR\mediaservice"
  RMDir /r "$INSTDIR\imageformats"
  RMDir /r "$INSTDIR\iconengines"
  RMDir /r "$INSTDIR\html"
  RMDir /r "$INSTDIR\bearer"
  RMDir /r "$INSTDIR\audio"
  
  RMDir /r "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  SetAutoClose true
SectionEnd

#-- 根据 NSIS 脚本编辑规则，所有 Function 区段必须放置在 Section 区段之后编写，以避免安装程序出现未可预知的问题。--#

;开始卸载时检查：
Function un.onInit
!insertmacro MUI_UNGETLANGUAGE
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "您确实要完全移除${PRODUCT_NAME}，及其所有的组件？" IDYES +2
  nsProcess::_FindProcess "kashow.exe"
  Pop $R0
  IntCmp $R0 0 running no_running no_running
  running:
  MessageBox MB_ICONQUESTION|MB_YESNO "安装程序检测到 ${PRODUCT_NAME} 正在运行，是否强行关闭并继续卸载?" IDYES dokill IDNO stopit
  no_running:
  GoTo endding
  dokill:
  nsProcess::_CloseProcess "kashow.exe"
  Pop $R0
  GoTo endding
  stopit:
  Abort
  endding:
  nsProcess::_Unload
FunctionEnd

Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) 已成功地从您的计算机移除。"
FunctionEnd
